<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 18/11/15
     * Time: 16:33
     */
    class User extends Controller
    {
        public function __construct() {
            parent::__construct();
            Session::init();
            $logged = Session::get('loggedIn');
            $role = Session::get('role');

            if ($logged == false || $role != 'owner') {
                Session::destroy();
                header('location: login');
                exit;
            }


        }




        public function index()
        {
            $this->view->userList = $this->model->userList();
            $this->view->render('user/index');
        }

        public function create(){
            $data = array();
            $data['login'] = $_POST['login'];
            $data['password'] = $_POST['password'];
            $data['password2'] = $_POST['password2'];
            $data['mail'] = $_POST['mail'];
            $data['role'] = $_POST['role'];
            $data['subscribe'] = $_POST['subscribe'];

            ///CHeck les erreurs

            $this->model->create($data);
            header('location:'.URL.'user');
        }

        public function edit($id){

            //On recupere d'abord les utilisateurs
            $this->view->user = $this->model->userSIngleList($id);
            $this->view->render('user/edit');

        }
        public function editSave($id){

            $data = array();
            $data['id'] = $id;
            $data['login'] = $_POST['login'];
            $data['password'] = $_POST['password'];
            $data['mail'] = $_POST['mail'];

            $this->model->editSave($data);
            header('location:'.URL.'user');
        }


        public function delete($id){
            $this->model->delete($id);
            header('location:'.URL.'user');
        }
    }
