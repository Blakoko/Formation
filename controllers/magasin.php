<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 29/11/15
     * Time: 21:11
     */
    class Magasin extends Controller
    {
     function __construct()
     {
         parent::__construct();
         $this->view->js = array('magasin/js/pajinate.js');
     }

        function index()
        {

                $this->view->magList = $this->model->magList();
                $this->view->render('magasin/index');

        }

            function single($id)
            {
                if(!empty($id)) {
                    $this->view->single = $this->model->magSingle($id);
                    $this->view->render('magasin/single');
                }
                else {
                    $this->index();
                }

            }

        function produit($id)
        {
            if(!empty($id)) {
                $this->view->prodSingle = $this->model->prodSingle($id);
                $this->view->render('magasin/produit');
            }
            else{
                $this->view->single($id);
            }
        }

        function ville()
        {
                $this->view->townlist  = $this->model->townlist();
                $this->view->render('magasin/index');
        }

        function bytown($id)
        {
            $this->view->magtown = $this->model->magtown($id);
            $this->view->render('magasin/town');
        }


    }