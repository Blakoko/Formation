<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 29/11/15
     * Time: 18:42
     */
    class Register extends Controller
    {
        public function __construct()
        {
            parent::__construct();
            Session::init();
            $logged = Session::get('loggedIn');
            if ($logged == true) {
                Session::destroy();
                header('location: register');
                exit;
            }
        }

        public function index()
    {
        $this->view->render('register/index');

    }
//Form::submit();
        public function create(){
            $data = array();
            $data['login'] = $_POST['login'];
            $data['password'] = $_POST['password'];
            $data['password2'] = $_POST['password2'];
            $data['mail'] = $_POST['mail'];

            ///CHeck les erreurs

            $this->model->create($data);
            $this->index();

        }


    }