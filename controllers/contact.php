<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 29/11/15
     * Time: 14:55
     */
    class Contact extends Controller
    {
        function __construct() {
            parent::__construct();
        }

        function index()
        {
            $this->view->render('contact/index');
        }

        function send()
        {
            $this->model->send();
        }


    }