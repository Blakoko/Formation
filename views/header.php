<?php Session::init(); ?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo URL?>public/css/main.css">

</head>


<body>
<header>
<div class="divheader">
    <img class="logo" src="<?php echo URL?>public/img/logo.png">
    <div class="divtitle">
        GSB - Formations
    </div>
</div>
<!---Debut Header-->
<?php if(Session::get('loggedIn') == true):?>
<div id="header">

    <nav role="navigation" class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
            </div>

            <div id="navbarCollapse" class="collapse navbar-collapse">
                <div class="nav navbar-nav">
                    <ul class="nav navbar-nav">

                            <li>
                                <a href="<?php echo URL ;?>index">Acceuil</a>
                            </li>
                            <li class="dropdown">
                            <?php if(Session::get('role') == 'owner'):?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Formation<span class="caret"></span></a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo URL ;?>formation">Apercu</a>
                                    </li>
                                    <li>
                                        <a href="#">Creer</a>
                                    </li>
                                    <li>
                                        <a href="#">Supprimer</a>
                                    </li>

                                    <li role="separator" class="divider"></li>

                                    <li>
                                        <a href="#">Modifier</a>
                                    </li>
                                </ul>
                            <?php endif;?>
                            <?php if(Session::get('role') == 'default'):?>
                                <li>
                                    <a href="<?php echo URL ;?>formation">Formation</a>
                                </li>
                            <?php endif;?>

                            </li>
                            <li class="dropdown">
                                <a href="<?php echo URL ;?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Compte<span class="caret"></span> </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo URL ;?>dashboard/index">Apercu</a>
                                    </li>
                                    <?php if(Session::get('role') == 'owner'):?>
                                        <li>
                                            <a href="#">Creer</a>
                                        </li>
                                        <li>
                                            <a href="#">Supprimer</a>
                                        </li>
                                    <?php endif; ?>
                                    <li>
                                        <a href="#">Modifier</a>

                                    </li>
                                    <li class="dropdown-header">
                                        <a href="<?php echo URL ;?>dashboard/profil/">Données</a>
                                        <?php if(Session::get('role') == 'owner'):?>
                                            <a href="#">Formation</a>
                                        <?php endif; ?>
                                    </li>

                                </ul>

                            </li>

                            <li> <a href="<?php echo URL ;?>dashboard/logout">Deconnexion</a></li>




                        <?php if(Session::get('role') == 'owner'):?>
                            <li>
                                <a href="<?php echo URL ;?>user">Edit User</a>
                            </li>

                        <?php endif; ?>




                    </ul>
                </div>
            </div>

        </div>
    </nav>


</div>
<!--Fin Header-->
<?php endif; ?>
</header>
<!--Content-->
<div class="cont">
    <div class="row">