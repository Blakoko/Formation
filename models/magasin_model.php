<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 29/11/15
     * Time: 21:26
     */
    class Magasin_Model extends Model
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function magList()
        {
            return $this->db->select("SELECT lib_mag,town.lib_town, magasin.id AS mag_id ,town.id AS town_id ,magasin.adress AS mag_adress FROM magasin,town WHERE id_town=town.id ");

        }

        public function magSingle($id)
        {
            return $this->db->select('SELECT * FROM  prod WHERE id_magasin =:id',array(':id'=>$id));
        }

        public function prodSingle($id)
        {
            return $this->db->select ('SELECT * FROM prod WHERE id=:id',array(':id'=>$id));
        }

        public function townlist()
        {
            return $this->db->select ('SELECT lib_town FROM town');
        }

        public function magtown($id)
        {
            return $this->db->select ('SELECT * FROM magasin WHERE lib_town =:id',array(':id'=>$id));
        }


    }