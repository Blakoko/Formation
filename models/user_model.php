<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 18/11/15
     * Time: 16:41
     */
    class User_Model extends Model
    {
        public function __construct()
        {
            parent::__construct();
        }

        /*Affiche la liste des utilisateurs  (ADmin)*/
        public function userList()
        {
            return $this->db->select('SELECT * FROM membre');

        }

        /*Affiche un utilisateur en fonction de son id (Admin)*/
        public function userSingleList($id)
        {
            return $this->db->select('SELECT id,mail,role FROM membre WHERE id=:id', array(':id'=> $id));


        }

        /*Creer un nouveau membre (Admin)*/
        public function create($data){

            $this->db->insert('membre',array(
                'login'  => $data['login'],
                'password' => Hash::create('sha1', $data['password'], HASH_PASSWORD_KEY),
                'mail'     => $data['mail'],
                //'role' => $data['role'],
                //'subscribe' =>$data['subscribe']
            ));
        }

        /*Supprimer un utilisateur (Admin)*/
        public function delete($id)
        {
            $data = $this->db->select('SELECT role FROM membre where id = :id',array(':id' => $id));
            if ($data[0]['role'] == 'owner') {
                return false;
            }
            $this->db->delete('membre',"id = '$id'");

        }

        /*Sauve les information de l'utilisateur apres les avoir editées (Admin)*/
        public function editSave($data){

            $postData = array(

                'login'=>$data['login'],
                'password' => Hash::create('sha1',$data['password'],HASH_PASSWORD_KEY),
                'role'=> Session::get('role'),
                'lastupdate' => date("Y-m-d H:i:s"),
            );

            $this->db->update('membre',$postData,"`id` = {$data['id']}");
        }


    }
