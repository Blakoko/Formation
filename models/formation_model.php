<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 27/02/16
     * Time: 22:20
     */
    class Formation_Model extends Model {

        /**
         * Formation_Model constructor.
         */
        public function __construct() {
            parent::__construct();

        }

        //Affiche la liste des formations//
        public function listformation()
        {
            return $this->db->select('SELECT *
            FROM formation
            JOIN categorie ON categorie_idCategorie = categorie.idCategorie
            JOIN formateur ON formateur_id_Formateur = formateur.id_Formateur');
        }

        public function verifinscription($id)
        {
            return $this->db->select('SELECT * FROM inscrire WHERE membre_id=:id',array(':id' =>$id));


        }
       //Affiche la liste des formations de l'utilisateur//
        public function formationSingleList($id)
        {
            return $this->db->select('SELECT * FROM formation,categorie,formateur WHERE id=:id GROUP BY idFormation', array(':id'=> $id));

        }

        //Affiche la liste des formateurs(pour le select).//
        public function listformateur()
        {
            return $this->db->select('SELECT * FROM formateur');
        }

        //Affiche la liste des categories (pour le select).//
        public function listcategorie()
        {
            return $this->db->select('SELECT * FROM categorie');
        }

        /**
        public function countplace($id)
        {
            return $this->db->select('SELECT COUNT(*) AS compte FROM inscrire WHERE idFormation = :id',array(':id'=>$id));
        }
         **/

        //Inscription a une formation via l'id de la formation et l'id de l'utilisateur (insertion en array)//
        public function inscription($data)
        {
            $this->db->insert('inscrire',array(
                'idFormation'=>$data['idFormation'],
                'membre_id'=>Session::get('id')

            ));
        }

        //Supprimer  une formation en par son id//
        public function delete($id)
        {

            if (Session::get('role') == 'default') {
                return false;
            }
            $this->db->delete('formation',"idFormation = '$id'");

        }

        //Apres edition de la formation sauvegarder les données (sous forme d'array)//
        public function editSave($data)
        {
            if (Session::get('role') == 'default') {
                return false;
            }
            $postData = array(
                'libelle_formation'=>$data['libelle_formation'],
                'date_formation'=>$data['date_formation'],
                'nbrplace'=>$data['nbrplace'],
                'formateur_id_formateur'=>$data['formateur_id_formateur'],
                'categorie_idCategorie' => $data['categorie_idCategorie']
            );

            $this->db->update('formation',$postData,"`id` = {$data['id']}");
        }

        //Function de creation de formation (envoyé sous forme d'array);//
        public function create($data)
        {
            if (Session::get('role') == 'default') {
                return false;
            }
            $this->db->insert('formation',array(
                'libelle_formation'=>$data['libelle_formation'],
                'date_formation'=>$data['date_formation'],
                'nbrplace'=>$data['nbrplace'],
                'formateur_id_formateur'=>$data['formateur_id_formateur'],
                'categorie_idCategorie' => $data['categorie_idCategorie']
            ));
        }

            //Affichage des details d'un formation

        public function details($id)
        {

            return $this->db->select('SELECT * FROM formation WHERE idFormation=:id', array(':id'=> $id));

        }

       /*public function register(){

                    $form = new form();

                    $form  ->post('name')
                        ->val('minlength', 2)
                        ->val('mail')

                        ->post('age')
                        ->val('minlength', 2)
                        ->val('digit')

                        ->post('mail')
                        ->val('mail')

                        ->post('gender');

                    $form  ->submit();

                    echo 'The form passed!';
                    $data = $form->fetch();

                    echo '<pre>';
                    print_r($data);
                    echo '</pre>';

                    $this->db->insert('person', $data);



        }*/
    }