<?php

    /**
     * Created by PhpStorm.
     * User: albert
     * Date: 17/11/15
     * Time: 19:10
     */
    class Login_Model extends Model
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function run()
        {
            $sth = $this->db->prepare("SELECT * FROM membre WHERE
				mail = :mail AND password = :password");
            $sth->execute(array(
                ':mail' => $_POST['mail'],
                ':password' => Hash::create('sha1',$_POST['password'],HASH_PASSWORD_KEY)
            ));



            $data = $sth->fetch();
            //print_r($data);
            //echo $data['role'];
            //die;
            //$data = $sth->fetchAll();



            $count =  $sth->rowCount();

            if ($count > 0) {
                // login
                Session::init();
                Session::set('id',$data['id']);
                Session::set('role',$data['role']);
                Session::set('login',$data['login']);
                Session::set('mail',$data['mail']);
                Session::set('loggedIn', true);
                header('location: ../dashboard');
            } else {
                header('location: '.URL.'/login');
            }
            //print_r($count);
        }

    }
